/**
This class extends autocomplete to use data with id
*/

var autocompleteData = function(options){

    var processedOptions = {
        urlQuery: function(term){return "/service?words="+encodeURI(term);}
        queryElement: "input[autocomplete]",
        minChars: 3,
        selectFunc : function(e){alert("Selected: " + e)}
        dataFormat:{
            id:"id",
            name:"name"
        }
    }
    for (var k in options) { if (options.hasOwnProperty(k)) processedOptions[k] = options[k]; }


    var _autoComplete = new autoComplete({
            selector: processedOptions.queryElement,
            minChars: processedOptions.minChars,
            source: function(term, suggest){
                term = term.toUpperCase()
                var htmlItem = $(this)
                $.ajax({
                        type       :"GET",
                        url        :processedOptions.urlQuery(term)
                        contentType:'application/json',
                        dataType   :'json',
                        beforeSend :function(){
                            modalLoading.show(htmlItem)
                            }
                        })
                        .always(function(){
                            modalLoading.hide(htmlItem)
                         })
                        .done(function(sourceData){
                            suggestData(htmlItem, sourceData, suggest)
                        })
                        .fail(function(xhr, strStatus, errorThrown){
                           suggest(["falled to get data"])
                        })
            }
        });

    var suggestData = new function(htmlItem, sourceData, suggest){
        let mapData = createSafeMapData(sourceData)

        $(item).data("autocomplete-suggest", mapData)

        suggest(getKeys(Object.keys(mapData)))
    }

    var createSafeMapData = function(sourceData){
        let mapCount = {}
        let mapData = {}
        sourceData.forEach((item)=>{
            let name = processedOptions.dataFormat.name
            let count = (mapCount[name]&0) + 1

            mapCount[name] = count

            if(count > 1){
                mapData[name + ' (' + count + ')'] = sourceData
            }else{
                mapData[name] = sourceData
            }
        })
    }
}