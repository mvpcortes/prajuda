( function(){
    var autoCompleteData = {
        data: [],
        updateData(sourceData){
            source = sourceData
                    .map((service)=>{
                        return {id:service.name, service:service}
                    })

        }

    }
    new autoComplete({
        selector: '#serviceSource',
        minChars: 3,
        source: function(term, suggest){
            term = term.toUpperCase()
            var item = $(this)
            $.ajax({
                    type       :"GET",
                    url        :"/service?words=" + encodeURI(term),
                    contentType:'application/json',
                    dataType   :'json',
                    beforeSend :function(){
                        modalLoading.show(item)
                        }
                    })
                    .always(function(){
                        modalLoading.hide(item)
                     })
                    .done(function(data){
                        suggest(data)
                    })
                    .fail(function(xhr, strStatus, errorThrown){
                       suggest(["falled to get data"])
                    })
        },
        renderItem: function (item, search){
            let label = item.documentDir + '-' + item.name
            return '<div class="autocomplete-suggestion" data-id="'+item.id+'" data-val="'+label+'">'+label+'</div>'
        },
        onSelect: function(e, term, item){
            $("#serviceSourceId").val($(item).data("id"))
        }

    });
}())