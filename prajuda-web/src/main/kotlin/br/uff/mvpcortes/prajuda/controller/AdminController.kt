package br.uff.mvpcortes.prajuda.controller

import br.uff.mvpcortes.prajuda.controller.helper.TemplateHelper
import org.springframework.stereotype.Controller
import org.springframework.transaction.annotation.Transactional
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping("admin")
class AdminController {

    @Transactional(readOnly = true)
    @GetMapping(value=["", "index.html"])
    fun index( model: Model):String{
        return TemplateHelper(model).withPage("fragments/admin/admin_index").apply()
    }
}