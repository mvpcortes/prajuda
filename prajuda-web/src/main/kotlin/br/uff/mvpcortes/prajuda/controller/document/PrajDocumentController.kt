package br.uff.mvpcortes.prajuda.controller.document

import br.uff.mvpcortes.prajuda.controller.helper.TemplateHelper
import br.uff.mvpcortes.prajuda.exception.BadRequestException
import br.uff.mvpcortes.prajuda.exception.PageNotFoundException
import br.uff.mvpcortes.prajuda.service.PrajDocumentService
import br.uff.mvpcortes.prajuda.service.PrajServiceService
import br.uff.mvpcortes.prajuda.service.PrajudaWorkerService
import org.springframework.format.Formatter
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.BeanPropertyBindingResult
import org.springframework.validation.beanvalidation.SpringValidatorAdapter
import org.springframework.web.bind.WebDataBinder
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.InitBinder
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.server.ServerErrorException
import org.thymeleaf.spring5.context.webflux.ReactiveDataDriverContextVariable
import reactor.core.publisher.toFlux
import java.util.*


/**
 * This controller mapping services names on prajuda URL and return its main page or docs page
 * @see {https://docs.spring.io/spring/docs/current/spring-framework-reference/web-reactive.html#webflux-ann-requestmapping}
 */
@Controller
class PrajDocumentController(val prajServiceService:PrajServiceService,
                             val prajudaWorkerService: PrajudaWorkerService,
                             val prajDocumentService: PrajDocumentService,
                             val springValidator: SpringValidatorAdapter){

    companion object {
        const val REGEX_VALID_SERVICE_NAME = "[\\w\\d_]+"
    }

    @GetMapping("document/{serviceName}.html")
    fun getMainPage(@PathVariable serviceName:String, model: Model):String{
        val service = prajServiceService.findIdByServiceNamePath(serviceName)
                ?: throw PageNotFoundException("Cannot found service $serviceName")

        return TemplateHelper(model)
                .withAttr("services",
                        ReactiveDataDriverContextVariable(prajServiceService.findByIdMono(service).toFlux(), 1))
                .withAttr("mapHarvesterType", prajudaWorkerService.mapHarvesterTypes())
                .withPage("fragments/service/service_show").apply()
    }


    class DocumentPathFormatter: Formatter<DocumentPath>{
        override fun parse(path: String, locale: Locale) = DocumentPath(path)

        override fun print(obj: DocumentPath, locale: Locale)= obj.toString()
    }

    @InitBinder
    fun initBinder(binder: WebDataBinder) {
        binder.addCustomFormatter(DocumentPathFormatter())
    }

    @GetMapping("document/{*documentPath}")
    fun getDocument(@PathVariable documentPath: DocumentPath,
                    model:Model):String{

        verifyDocumentPath(documentPath)

        if(!prajDocumentService.existsByServiceAndPath(documentPath.serviceName, documentPath.path)){
            throw PageNotFoundException("document not exists")
        }

        return TemplateHelper(model)
                .withAttr("documents", ReactiveDataDriverContextVariable(
                        prajDocumentService.findByServiceAndPathFlux(documentPath.serviceName, documentPath.path)
                ))
                .withPage("fragments/document/document_show")
                .apply()
    }


    fun verifyDocumentPath(documentPath: DocumentPath) {
        val bindingResult = BeanPropertyBindingResult(documentPath, "documentPath")
        springValidator.validate(documentPath, bindingResult)

        if (bindingResult.hasErrors()) {
            throw BadRequestException("Invalid document path: ${documentPath.toString()}. Should be document/directory/file.extension")
        }
    }

}