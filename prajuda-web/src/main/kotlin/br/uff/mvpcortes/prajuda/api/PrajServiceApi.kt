package br.uff.mvpcortes.prajuda.api

import br.uff.mvpcortes.prajuda.api.dto.toMonoResponseEntityErrorMessages
import br.uff.mvpcortes.prajuda.model.PrajService
import br.uff.mvpcortes.prajuda.model.WithId
import br.uff.mvpcortes.prajuda.service.PrajServiceService
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import org.springframework.web.bind.support.WebExchangeBindException
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@RestController()
@RequestMapping("service")
class PrajServiceApi( private val prajServiceService: PrajServiceService) {

    data class ServiceDTO(val id:String, val name:String, val documentDir:String, val url:String){
        constructor(service:PrajService):this(service.id!!, service.name, service.documentDir, service.url)
    }

    @GetMapping(produces = [APPLICATION_JSON_VALUE])
    fun findForAutocomplete(@RequestParam words:String): Flux<ServiceDTO> {
        return prajServiceService.findAutocompleteLimit10(words)
                .map(::ServiceDTO)
    }

    @PostMapping()
    fun save(@Validated @RequestBody monoPrajService:Mono<PrajService>):Mono<ResponseEntity<WithId>>{
        return monoPrajService
                .map{ prajServiceService.save(it).onlyId() }
                .map{ ResponseEntity.ok(it) }
                .onErrorResume({ ex-> ex is WebExchangeBindException}, {
                    (it as WebExchangeBindException).bindingResult.toMonoResponseEntityErrorMessages<WithId>()
                })
    }
}