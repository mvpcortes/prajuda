package br.uff.mvpcortes.prajuda.controller.document

import br.uff.mvpcortes.prajuda.model.PrajService
import br.uff.mvpcortes.prajuda.model.validation.RelativePath
import java.util.stream.Collectors
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern

class DocumentPath(path:String){
    val splited = removeFirstPath(path).split("/")

    private fun removeFirstPath(path: String): String {
        return if(path.isNotEmpty() && path[0] == '/'){
            path.substring(1)
        }else{
            path
        }
    }

    @field:Pattern(
            regexp= PrajService.REGEX_PATH_NAME_VALIDATION,
            message = "{br.uff.mvpcortes.prajuda.model.PrajService.namePath.message}")
    @field:NotNull
    val serviceName = if(splited.size<=1) {""} else { splited[0] } //if has <=2 elements it should not have service.

    @field:RelativePath
    val path = splited.stream()
            .skip(if(splited.size<=1){0}else{1})
            .collect(Collectors.joining("/"))
            .substringBeforeLast(".")

    @field:Pattern(regexp = "(html)|()")
    val extension = splited.last().let {
        it.takeIf { it.contains(".") }
                ?.let { it.substringAfterLast(".") }
                ?: ""
    }

    override fun toString(): String {
        return "$serviceName/$path.$extension"
    }
}