package br.uff.mvpcortes.prajuda.controller

import br.uff.mvpcortes.prajuda.controller.helper.PageRequest
import br.uff.mvpcortes.prajuda.controller.helper.TemplateHelper
import br.uff.mvpcortes.prajuda.controller.helper.pagination.Pagination
import br.uff.mvpcortes.prajuda.dao.HarvestRequestDAO
import br.uff.mvpcortes.prajuda.model.HarvestRequest
import br.uff.mvpcortes.prajuda.model.HarvestType
import org.springframework.stereotype.Controller
import org.springframework.transaction.annotation.Transactional
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.thymeleaf.spring5.context.webflux.ReactiveDataDriverContextVariable

@Controller
@RequestMapping("harvest_request")
class HarvestRequestController(private val harvestRequestDAO: HarvestRequestDAO) {

    @Transactional(readOnly = true)
    @GetMapping(value=["", "list.html"])
    fun list(pageRequest:PageRequest, model: Model):String{
        val harvestRequests    = harvestRequestDAO.findPages(pageRequest.page-1, pageRequest.pageSize)
        val qtdService = harvestRequestDAO.count()
        val pagination = Pagination((qtdService/pageRequest.pageSize).toInt(), pageRequest.page)


        model.addAttribute("harvest_requests", ReactiveDataDriverContextVariable(harvestRequests))
        model.addAttribute("pagination", pagination)
        model.addAttribute("qtd_services", qtdService)
        model.addAttribute("pageRequest", pageRequest)


        return TemplateHelper(model).withPage("fragments/harvest_request/harvest_request_list").apply()
    }

    @GetMapping("new")
    fun new(model:Model)=
        TemplateHelper(model)
                .withAttrNotNull("harvestTypes", HarvestType.values())
                .withAttrNotNull("harvestRequest", HarvestRequest())
                .withPage("fragments/harvest_request/harvest_request_new")
                .apply()

}