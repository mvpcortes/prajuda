package br.uff.mvpcortes.prajuda.api

import br.uff.mvpcortes.prajuda.api.dto.toMonoResponseEntityErrorMessages
import br.uff.mvpcortes.prajuda.dao.HarvestRequestDAO
import br.uff.mvpcortes.prajuda.model.HarvestRequest
import br.uff.mvpcortes.prajuda.model.WithId
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.support.WebExchangeBindException
import reactor.core.publisher.Mono

@RestController()
@RequestMapping("harvest_request")
class HarvestRequestApi( private val harvestRequestDAO: HarvestRequestDAO) {


    @PostMapping
    fun save(@Validated @RequestBody  monoHarvestRequest: Mono<HarvestRequest>)=
            monoHarvestRequest
                    .map { harvestRequestDAO.save(it).onlyId()}
                    .map{ ResponseEntity.ok(it) }
                    .onErrorResume({ ex-> ex is WebExchangeBindException }, {
                        (it as WebExchangeBindException).bindingResult.toMonoResponseEntityErrorMessages<WithId>()
                    })

}
