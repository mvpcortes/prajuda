package br.uff.mvpcortes.prajuda.api

import br.uff.mvpcortes.prajuda.api.PrajServiceApi.ServiceDTO
import br.uff.mvpcortes.prajuda.model.PrajService
import br.uff.mvpcortes.prajuda.service.PrajServiceService
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.whenever
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.core.publisher.Flux

@ExtendWith(value=[SpringExtension::class])
@WebFluxTest(PrajServiceApi::class)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class PrajServiceApiTest {

    @Autowired
    lateinit var client: WebTestClient

    @MockBean
    private lateinit var prajServiceService: PrajServiceService

    @Test
    fun `when get dto services then call prajService`(){
        doReturn(createPrajServiceFlux())
                .whenever(prajServiceService)
                .findAutocompleteLimit10("xuxu")

        client.get()
                .uri("/service?words=xuxu")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(ServiceDTO::class.java)
                .contains(*createPrajServices().map{ServiceDTO(it)}.toTypedArray())
    }


    private fun createPrajServiceFlux() = Flux.just(
            *createPrajServices()
                    .toTypedArray()
    )

    private fun createPrajServices() = (1..10)
            .map { PrajService(id = it.toString(), name = "NAME_$it") }
}