package br.uff.mvpcortes.prajuda.view

import io.github.bonigarcia.seljup.Options
import org.openqa.selenium.Dimension
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.springframework.boot.web.server.LocalServerPort
import java.util.concurrent.TimeUnit
import java.util.HashMap




open abstract class AbstractViewTest {

    @LocalServerPort
    var port:Int = 0


    @Options
    val chromeOptions = createChromeOptions()

    fun createChromeOptions(): ChromeOptions {
        val prefs = HashMap<String, Any>()

        prefs["intl.accept_languages"] = "en-us"

        return ChromeOptions()
        .setExperimentalOption("prefs", prefs)
        .setHeadless(true)
        .addArguments("--lang=en-us"," --window-size=360x640")
    }


    fun get(webDriver: WebDriver, path:String): WebDriver {

        //resize window to mobile form (mobile-first
        webDriver.manage().window().size = Dimension(360, 640)
        webDriver.get("http://localhost:$port/$path")

        //implicit wait
        webDriver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS)
        return webDriver
    }

}