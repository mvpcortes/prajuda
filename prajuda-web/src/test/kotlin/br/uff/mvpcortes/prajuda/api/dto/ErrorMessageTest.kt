package br.uff.mvpcortes.prajuda.api.dto

import br.uff.mvpcortes.prajuda.model.fixture.PrajDocumentFixture
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.validation.BindException
import org.springframework.validation.DirectFieldBindingResult
import org.springframework.validation.ObjectError

internal class ErrorMessageTest{

    @Nested
    inner class `when use constructor with ObjectError`{
        val objectError = ObjectError(
                "xuxu",
                null,
                 null,
                "default")

        val errorMessage = ErrorMessage(objectError)

        @Test
        fun `then field is equal to objectName`() {
            assertThat(errorMessage.field).isEqualTo("xuxu")
        }

        @Test
        fun `then message is equal to defaultMessage`() {
            assertThat(errorMessage.message).isEqualTo(objectError.defaultMessage)
        }

        @Test
        fun `then invalid value is null`() {
            assertThat(errorMessage.invalidValue).isNull()
        }

        @Test
        fun `when get error messages on empty bindingResult then return null`(){
           val bindResult = BindException("a", "a")
           val messages = bindResult.toErrorMessages()

            assertThat(messages).isNull()
        }

        @Test
        fun `when get error messages on field bindingResult then return not null`(){
            val bindResult = BindException("a", "a")
            bindResult.addError(ObjectError("a","a"))
            val messages = bindResult.toErrorMessages()!!

            assertThat(messages).isNotNull
            assertThat(messages["a"]).isEqualTo(ErrorMessage(field = "a", message="a", invalidValue = null))
        }
    }
}