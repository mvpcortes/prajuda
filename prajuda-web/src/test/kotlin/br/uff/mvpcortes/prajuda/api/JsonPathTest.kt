package br.uff.mvpcortes.prajuda.api

import com.jayway.jsonpath.Configuration
import com.jayway.jsonpath.JsonPath
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class JsonPathTest {
    val JSON_SOURCE =
            """
                [
                  {
                    "id": "1",
                    "name": "prajuda",
                    "url": "https://app.uff.br/rad"
                  },
                  {
                    "id": "2",
                    "name": "google",
                    "url": "https://www.google.com"
                  },
                  {
                    "id": "3",
                    "name": "bing",
                    "url": "https://www.bing.com"
                  }
                ]
            """.trimIndent()

    @Test
    fun `when get 1rt element then return 1 element`(){
        val json = Configuration.defaultConfiguration().jsonProvider().parse(JSON_SOURCE)
        val result = JsonPath.read<Any>(json, "$.[0]").toString()
        Assertions.assertThat(result).isEqualTo("{id=1, name=prajuda, url=https://app.uff.br/rad}")
    }

    @Test
    fun `when get 2nd element then return one element`(){
        val json = Configuration.defaultConfiguration().jsonProvider().parse(JSON_SOURCE)
        val result = JsonPath.read<Any>(json, "$.[1]").toString()
        Assertions.assertThat(result).isEqualTo("{id=2, name=google, url=https://www.google.com}")
    }

    @Test
    fun `when get 3td element element then return one element`(){
        val json = Configuration.defaultConfiguration().jsonProvider().parse(JSON_SOURCE)
        val result = JsonPath.read<Any>(json, "$.[2]").toString()
        Assertions.assertThat(result).isEqualTo("{id=3, name=bing, url=https://www.bing.com}")
    }

    @Test
    fun test(){
        val str = "[{\"id\":\"1\",\"name\":\"NAME_1\",\"url\":\"n/a\"},{\"id\":\"2\",\"name\":\"NAME_2\",\"url\":\"n/a\"},{\"id\":\"3\",\"name\":\"NAME_3\",\"url\":\"n/a\"},{\"id\":\"4\",\"name\":\"NAME_4\",\"url\":\"n/a\"},{\"id\":\"5\",\"name\":\"NAME_5\",\"url\":\"n/a\"},{\"id\":\"6\",\"name\":\"NAME_6\",\"url\":\"n/a\"},{\"id\":\"7\",\"name\":\"NAME_7\",\"url\":\"n/a\"},{\"id\":\"8\",\"name\":\"NAME_8\",\"url\":\"n/a\"},{\"id\":\"9\",\"name\":\"NAME_9\",\"url\":\"n/a\"},{\"id\":\"10\",\"name\":\"NAME_10\",\"url\":\"n/a\"}]"
        val json = Configuration.defaultConfiguration().jsonProvider().parse(JSON_SOURCE)
        val result = JsonPath.read<Any>(json, "$.[1]").toString()
        println(result);

    }

    @Test
    fun `when get list then return exactly source`(){
        val json = Configuration.defaultConfiguration().jsonProvider().parse(JSON_SOURCE)
        val result = JsonPath.read<Any>(json, "$").toString()
        Assertions.assertThat(result).containsSubsequence(
                "[",
                "{","\"id\"", "1","}",
                "{","\"id\"", "2","}",
                "{","\"id\"", "3","}",
                "]"
        )
    }

    @Test
    fun `when get size or array then return number`(){
        val json = Configuration.defaultConfiguration().jsonProvider().parse(JSON_SOURCE)
        val result = JsonPath.read<Any>(json, "$.length()").toString()
        Assertions.assertThat(result).containsSubsequence(
               "3"
        )
    }
}