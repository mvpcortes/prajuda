package br.uff.mvpcortes.prajuda.service

import br.uff.mvpcortes.prajuda.dao.PrajServiceDAO
import br.uff.mvpcortes.prajuda.model.PrajService
import br.uff.mvpcortes.prajuda.model.fixture.PrajServiceFixture
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import reactor.test.StepVerifier

class PrajServiceServiceTest{

    val prajServiceDAO : PrajServiceDAO = mock{}

    var prajServiceService = PrajServiceService(prajServiceDAO)

    @Test
    fun `when save then delegate to DAO`(){
        val prajService = PrajServiceFixture.withName("a")
        prajServiceService.save(prajService)

        verify(prajServiceDAO).save(prajService)
    }



    @Test
    fun `when findById then delegate to DAO`(){
        val prajService = PrajServiceFixture.withName("a")
        prajServiceService.findById("1")

        verify(prajServiceDAO).findByIdNullable("1")
    }


    @Test
    fun `when findServices then delegate to DAO`(){
        val prajService = PrajServiceFixture.withName("a")
        prajServiceService.findServices(0, 10)

        verify(prajServiceDAO).findPage(0, 10)
    }

    @Test
    fun `when count then delegate to DAO`(){
        val prajService = PrajServiceFixture.withName("a")
        prajServiceService.count()

        verify(prajServiceDAO).count()
    }

    @Test
    fun `when findByName then delegate to DAO`(){
        val prajService = PrajServiceFixture.withName("a")
        prajServiceService.findByName(prajService.name)

        verify(prajServiceDAO).findByName(prajService.name)
    }



    abstract inner class NServices(val qtd:Int){

        var listServices:List<PrajService> = ArrayList(0)



        @AfterEach
        fun destroy(){
            listServices.forEach {
                prajServiceDAO.delete(it.id!!)
            }
        }

        fun createNServices(qtd:Int):List<PrajService>{
            return (1..qtd)
                    .map{PrajServiceFixture.withName("PRAJ SERVICE FOR TEST $it")}
                    .map(prajServiceDAO::save)
        }
    }

    @Nested
    inner class `when have 10 services`:NServices(10){

        @Test
        fun `then return 10 services with name like 'SERVICE'`(){
            val services = prajServiceService.findAutocompleteLimit10("SERVICE")
            StepVerifier.create(services)
                    .expectNext(*listServices.toTypedArray())
                    .expectComplete()
        }
    }

}