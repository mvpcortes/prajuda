package br.uff.mvpcortes.prajuda.view

import br.uff.mvpcortes.prajuda.dao.PrajDocumentDAO
import br.uff.mvpcortes.prajuda.dao.PrajServiceDAO
import br.uff.mvpcortes.prajuda.model.PrajDocument
import br.uff.mvpcortes.prajuda.model.PrajService
import br.uff.mvpcortes.prajuda.model.fixture.PrajDocumentFixture
import br.uff.mvpcortes.prajuda.model.fixture.PrajServiceFixture
import io.github.bonigarcia.seljup.SeleniumExtension
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.openqa.selenium.By
import org.openqa.selenium.chrome.ChromeDriver
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.transaction.annotation.Transactional

@ExtendWith(value=[SpringExtension::class, SeleniumExtension::class])
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DocumentViewTest:AbstractViewTest(){

    @Autowired
    lateinit var prajServiceDAO: PrajServiceDAO

    @Autowired
    lateinit var prajDocumentDAO: PrajDocumentDAO

    val SERVICE_NAME = "my_service_view"

    val DOCUMENT_PATH = "project/test"

    val DOCUMENT_CONTENT = """
        # TEST
        It is my test **123**. lorem ypsilon. lorem ypsilon. lorem ypsilon. lorem ypsilon.
        ```bash
        git clone test
        ```
        ## sub-test
        My sub-test.
    """.trimIndent()

    var prajService: PrajService = PrajService()
    var documentA : PrajDocument = PrajDocument()

    @Transactional
    @BeforeAll
    fun initData() {
        prajService = PrajServiceFixture.withName(SERVICE_NAME)
        prajServiceDAO.save(prajService)

        //
        documentA = PrajDocumentFixture.new(
                prajService = prajService,
                path = DOCUMENT_PATH,
                content = DOCUMENT_CONTENT)
        prajDocumentDAO.save(documentA)
    }

    @Transactional
    @AfterAll
    fun deleteData() {
        prajDocumentDAO.delete(documentA)
        prajServiceDAO.delete(prajService.id!!)
    }

    @Test
    fun `when try access a invalid_path then we not found Bad Request 400`(webDriver: ChromeDriver){
        get(webDriver, "document/xuxu.xaxa.lalala_/ovovovov_alalal.html")

        val element = webDriver.findElements(By.id("error_section"))
                .stream()
                .findFirst()
                .orElseThrow { IllegalStateException("Cannot found error_section") }

        val htmlContent = element.getAttribute("innerHTML")

        assertThat(htmlContent)
                .containsSubsequence(
                        "<img alt=\"Prajuda logo\"",
                        "images/logo/prajuda_min.png",
                        ">",
                        "<h2 id=\"status\" class=\"title has-text-centered\">400</h2>",
                        "<p id=\"error\" class=\"subtitle has-text-centered\"", "Bad Request", "</p>"
                )
    }

    @Test
    fun `when try acess a not exists document then we not found 404`(webDriver: ChromeDriver){
        get(webDriver, "document/$SERVICE_NAME/lalalalalalaalalal.html")

        val element = webDriver.findElements(By.id("error_section"))
                .stream()
                .findFirst()
                .orElseThrow { IllegalStateException("Cannot found error_section") }

        val htmlContent = element.getAttribute("innerHTML")

        assertThat(htmlContent)
                .containsSubsequence(
                        "<img alt=\"Prajuda logo\"",
                        "images/logo/prajuda_min.png",
                        ">",
                        "<h2 id=\"status\" class=\"title has-text-centered\">404</h2>",
                        "<p id=\"error\" class=\"subtitle has-text-centered\"", "Not Found", "</p>"
                )
    }


    @Test
    fun `when a service 'my_service_view' and a document 'project test' then we can access url document`(webDriver: ChromeDriver){
        get(webDriver, "document/$SERVICE_NAME/$DOCUMENT_PATH.html")


        val element = webDriver.findElements(By.id("document_${documentA.id}"))
                .stream()
                .findFirst()
                .orElseThrow { IllegalStateException("Cannot found document_${documentA.id}") }


        val htmlContent = element.getAttribute("innerHTML")

        assertThat(htmlContent)
                .containsSubsequence(
                        "<h1>","TEST", "</h1>",
                        "<p>",
                        "It is my test",
                        "<strong>", "123", "</strong>",
                        "lorem ypsilon. lorem ypsilon. lorem ypsilon. lorem ypsilon.",
                        "</p>",
                        "<code class=\"language-bash\">", "git clone test", "</code>",
                        "<h2>","sub-test","</h2>",
                        "<p>", "My sub-test.","</p>"
                )
    }
}