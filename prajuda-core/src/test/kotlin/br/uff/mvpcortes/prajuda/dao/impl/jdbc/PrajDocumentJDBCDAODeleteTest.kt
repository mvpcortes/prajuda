package br.uff.mvpcortes.prajuda.dao.impl.jdbc

import br.uff.mvpcortes.prajuda.model.PrajDocument
import br.uff.mvpcortes.prajuda.model.PrajService
import br.uff.mvpcortes.prajuda.model.fixture.PrajDocumentFixture
import br.uff.mvpcortes.prajuda.model.fixture.PrajServiceFixture
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.Rollback
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.transaction.annotation.Transactional

@ExtendWith(SpringExtension::class)
@SpringBootTest()
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DisplayName("When PrajDocument is a new saved document")
class PrajDocumentJDBCDAODeleteTest {
    @Autowired
    lateinit var prajServiceJDBCDAO: PrajServiceJDBCDAO

    @Autowired
    lateinit var prajDocumentJDBCDAO: PrajDocumentJDBCDAO


    var prajService : PrajService? = null

    @BeforeAll
    fun initAll(){
        prajService =  PrajServiceFixture.withName("XUXU")
        prajServiceJDBCDAO.save(prajService!!)

    }

    var prajDocumentBlank : PrajDocument? = null

    @BeforeEach
    fun init(){
        prajDocumentBlank = PrajDocumentFixture.new(prajService=prajService!!)
        prajDocumentJDBCDAO.save(prajDocumentBlank!!)
    }

    @AfterEach
    fun destroy(){
        prajDocumentJDBCDAO.delete(prajDocumentBlank!!)
    }

    @Test
    fun `and delete it then will not exists in database`(){
        prajDocumentJDBCDAO.delete(prajDocumentBlank!!)

        Assertions.assertThat(prajDocumentJDBCDAO.findById(prajDocumentBlank!!.id!!)).isNull()
    }

    @Transactional
    @Test
    fun `and delete it then count is less then before`(){
        val qtd = prajDocumentJDBCDAO.count()
        prajDocumentJDBCDAO.delete(prajDocumentBlank!!)

        val newQtd = prajDocumentJDBCDAO.count()
        Assertions.assertThat(qtd).isGreaterThan(newQtd)
    }

    @Test
    @Transactional
    @Rollback
    fun `and delete all documents then has zero documents`(){

        val qtd = prajDocumentJDBCDAO.count()

        prajDocumentJDBCDAO.deleteAll()

        val newQtd = prajDocumentJDBCDAO.count()

        Assertions.assertThat(prajDocumentJDBCDAO.findById(prajDocumentBlank!!.id!!)).isNull()
        Assertions.assertThat(qtd).isGreaterThanOrEqualTo(newQtd)
        Assertions.assertThat(newQtd).isEqualTo(0L)
    }


    @Test
    fun `and delete by tracking service and path`(){

        val returned = prajDocumentJDBCDAO.deleteTrackingServiceAndPath(this.prajDocumentBlank!!)

        Assertions.assertThat(prajDocumentJDBCDAO.findById(this.prajDocumentBlank!!.id!!)).isNull()

        Assertions.assertThat(returned).isGreaterThan(0)
    }

    @Test
    fun `and delete by tracking service and path but not exists then fail`(){

        val documentChanged = prajDocumentBlank!!.copy(serviceId = "1209321980321908")
        val returned = prajDocumentJDBCDAO.deleteTrackingServiceAndPath(documentChanged)

        Assertions.assertThat(returned).isEqualTo(0)
    }
}