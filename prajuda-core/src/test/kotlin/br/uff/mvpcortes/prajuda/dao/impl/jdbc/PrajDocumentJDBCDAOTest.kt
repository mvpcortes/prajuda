package br.uff.mvpcortes.prajuda.dao.impl.jdbc

import br.uff.mvpcortes.prajuda.model.PrajDocument
import br.uff.mvpcortes.prajuda.model.PrajService
import br.uff.mvpcortes.prajuda.model.fixture.PrajDocumentFixture
import br.uff.mvpcortes.prajuda.model.fixture.PrajServiceFixture
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest()
@TestInstance(TestInstance.Lifecycle.PER_METHOD)
class PrajDocumentJDBCDAOTest{

    @Autowired
    lateinit var prajServiceJDBCDAO: PrajServiceJDBCDAO

    @Autowired
    lateinit var prajDocumentJDBCDAO: PrajDocumentJDBCDAO


    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class `when save a document`{

        var prajService : PrajService? = null

        @BeforeAll
        fun init(){
            prajService =  PrajServiceFixture.withName("XUXU")
            prajServiceJDBCDAO.save(prajService!!)

        }

        @AfterAll
        fun drop(){
            prajDocumentJDBCDAO.deleteByServiceId(prajService!!.id!!)
            prajServiceJDBCDAO.delete(prajService!!.id!!)
        }


        @Test
        fun `when try save a document with tracking service and path then ok`(){

            val prajDocument :PrajDocument= PrajDocumentFixture.new(prajService=prajService!!)
            val FIRST_NAME  = "name_will_be_changed"
            val SECOND_NAME = "changed"
            prajDocument.path = FIRST_NAME
            prajDocumentJDBCDAO.save(prajDocument)

            prajDocument.path = SECOND_NAME
            prajDocumentJDBCDAO.saveTrackingServiceAndPath(prajDocument)

            assertThat(prajDocument.path).isNotEqualTo(FIRST_NAME)
            assertThat(prajDocument.path).isEqualTo(SECOND_NAME)

        }

        @Test
        fun `and try find a prajDocument id by service and path then not found`() {
            val idDocument = prajDocumentJDBCDAO
                    .findIdByServiceAndPath(
                            this.prajService!!.id!!,
                            "xuxuxaxa120809808")

            assertThat(idDocument).isNull()
        }

        @Test
        fun `and try find a prajDocument by service and path then not found`() {
            val document = prajDocumentJDBCDAO
                    .findByServiceNamePathAndPath(
                            this.prajService!!.namePath!!,
                            "xuxuxaxa120809808")
            assertThat(document).isNull()
        }

        @Test
        fun `and try exists a prajDocument by service and path then not found`(){
            val document = prajDocumentJDBCDAO
                    .existsByServiceNamePathAndPath(this.prajService!!.namePath!!,
                            "xuxuxaxa120809808")
            assertThat(document).isFalse()
        }

        @Test
        fun `and try save a new document with id blank then create new id`(){
            val prajDocumentBlank :PrajDocument= PrajDocumentFixture.new(prajService=prajService!!)
            prajDocumentBlank.id = "    "
            prajDocumentJDBCDAO.save(prajDocumentBlank)

            assertThat(prajDocumentBlank.id).isNotNull()
        }

        @Test
        fun `and try test exists a saved document then return true`(){
            val prajDocumentBlank :PrajDocument= PrajDocumentFixture.new(prajService=prajService!!)
            prajDocumentJDBCDAO.save(prajDocumentBlank)

            val exists = prajDocumentJDBCDAO
                    .existsByServiceNamePathAndPath(
                            prajService!!.namePath,
                            prajDocumentBlank.path)

            assertThat(exists).isTrue()
        }

        @Test
        fun `and try test exists a saved document then return document`(){
            val prajDocumentBlank :PrajDocument= PrajDocumentFixture.new(prajService=prajService!!)
            prajDocumentJDBCDAO.save(prajDocumentBlank)

            val document = prajDocumentJDBCDAO
                    .findByServiceNamePathAndPath(
                            prajService!!.namePath,
                            prajDocumentBlank.path)

            assertThat(document).isEqualTo(prajDocumentBlank)
        }

        @Test
        fun `when update a document then ok`(){
            val prajDocument = PrajDocumentFixture.new(prajService = prajService!!)
            prajDocumentJDBCDAO.save(prajDocument)

            val prajDocumentChanged = prajDocument.copy(content="changed", path="a/b/c")

            prajDocumentJDBCDAO.save(prajDocumentChanged)

            val prajDocumentSaved = prajDocumentJDBCDAO.findById(prajDocument.id!!)!!

            assertThat(prajDocumentChanged.id).isEqualTo(prajDocumentSaved.id)
            assertThat(prajDocumentChanged.serviceId).isEqualTo(prajDocumentSaved.serviceId)
            assertThat(prajDocumentChanged.serviceName).isEqualTo(prajDocumentSaved.serviceName)
            assertThat(prajDocument.path).isNotEqualTo(prajDocumentSaved.path)
            assertThat(prajDocument.content).isNotEqualTo(prajDocumentSaved.content)
        }
    }
}