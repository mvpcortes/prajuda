package br.uff.mvpcortes.prajuda.service

import br.uff.mvpcortes.prajuda.dao.PrajServiceDAO
import br.uff.mvpcortes.prajuda.model.PrajService
import br.uff.mvpcortes.prajuda.model.fixture.PrajServiceFixture
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import reactor.test.StepVerifier
import java.util.stream.Collectors.toList

@ExtendWith(SpringExtension::class)
@SpringBootTest()
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PrajServiceServiceIntegrationTest{

    @Autowired
    lateinit var prajServiceDAO:PrajServiceDAO

    @Autowired
    lateinit var prajServiceService:PrajServiceService

    abstract inner class NServices(val qtd:Int) {

        var listServices: List<PrajService> = ArrayList(0)
        var listFalseServices: List<PrajService> = ArrayList(0)


        @BeforeEach
        fun init() {
            listServices = createNServices(qtd)
            listFalseServices = createNFalseServices(3)
        }

        @AfterEach
        fun destroy() {
            listServices.forEach { prajServiceDAO.delete(it.id!!) }
            listFalseServices.forEach { prajServiceDAO.delete(it.id!!) }
        }


        fun createNServices(qtd:Int):List<PrajService>{
            return (1..qtd)
                    .map{ PrajServiceFixture.withNameAndUrlAndDocDir(
                            name="PRAJ 56567 SERVICE FOR TEST PATH PATH 4445 $it",
                            url="url://aaattt43dg_$it/xuxu",
                            documentDir = "dir/doc/456534rt5643/")}
                    .map(prajServiceDAO::save)
                    .sortedBy { it.name }
        }

        fun createNFalseServices(qtd:Int):List<PrajService>{
            return (1..qtd)
                    .map{ PrajServiceFixture.withNameAndUrlAndDocDir(
                            name="PRAJ xxxxxx SERVICE FOR TEST PATH PATH 4445 $it",
                            url="url://xxxxxx_$it/xuxu",
                            documentDir = "dir/doc/xxxxxxxx_/")}
                    .map(prajServiceDAO::save)
                    .sortedBy { it.name }
        }
    }

    abstract inner class NServicesWithTest(qtd:Int):NServices(qtd){

        fun getLimitedList(list:List<PrajService>) = list.take(10)

        @Test
        fun `and query by name then return N services with name like 'SERVICE'`(){
            val services = prajServiceService.findAutocompleteLimit10("56567")
            StepVerifier.create(services)
                    .expectNext(*getLimitedList(listServices).toTypedArray())
                    .expectComplete()
        }

        @Test
        fun `and query by name_path then return N services with path like 'PATH_PATH_4445'`(){
            val services = prajServiceService.findAutocompleteLimit10("PATH_PATH_4445")
            StepVerifier.create(services)
                    .expectNext(*getLimitedList(listServices).toTypedArray())
                    .expectComplete()
        }

        @Test
        fun `and query by url then return N services with url like 'aaattt43dg_'`(){
            val services = prajServiceService.findAutocompleteLimit10("aaattt43dg_")
            StepVerifier.create(services)
                    .expectNext(*getLimitedList(listServices).toTypedArray())
                    .expectComplete()
        }

        @Test
        fun `and query by document_dir then return N services with document_dir like 'aaattt43dg_'`(){
            val services = prajServiceService.findAutocompleteLimit10("456534rt5643")
            StepVerifier.create(services)
                    .expectNext(*getLimitedList(listServices).toTypedArray())
                    .expectComplete()
        }
    }

    @Nested
    inner class `when have 10 services`:NServicesWithTest(10)

    @Nested
    inner class `when have 5 services`:NServicesWithTest(5)


    @Nested
    inner class `when have 20 services`:NServicesWithTest(20)

}