package br.uff.mvpcortes.prajuda.config

import br.uff.mvpcortes.prajuda.workdir.WorkDirectoryProviderTestImpl
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Primary

@TestConfiguration
class WorkerTestConfiguration {

    @Primary
    @Bean
    fun workerDirectoryProviderTest() = WorkDirectoryProviderTestImpl()
}