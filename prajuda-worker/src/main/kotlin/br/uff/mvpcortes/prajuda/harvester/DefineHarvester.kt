package br.uff.mvpcortes.prajuda.harvester

import org.springframework.core.annotation.AliasFor

annotation class DefineHarvester(
        @get:AliasFor(annotation = DefineHarvester::class, attribute = "name")val value:String="",
        @get:AliasFor(annotation = DefineHarvester::class, attribute = "value")val name:String="",
        val id:String="")